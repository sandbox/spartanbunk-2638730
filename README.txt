Thank you for installing the YouTube Video Background module.

This module enables you to quickly deploy YouTube videos to the background of your site.

1. Create a div in your .tpl file with an ID of 'youtube_wrapper' that wraps all your content.
	<div id="youtube_wrapper">YOUR WEBSITES HTML</div>

2. Add the following rule to your style sheet with the following styles:

#youtube_wrapper{	
	width:100%;
	height:auto;
	margin:0 auto;
	overflow-y:hidden;}

3. Go to admin/config/youtube_bg/manage to enter your YouTube video's ID number.
	It is the series of letters and numbers after the "=" sign.
	For example the video id in this URL (https://www.youtube.com/watch?v=ROKU5Etckbk) is "ROKU5Etckbk".  

4. Click "Save configuration" and refresh the page.

*************NOTE TO THE DEVELOPER**************

YouTube videos will automatically restart when you refresh or navigate to a new page so this module is made to make your website one page with page refreshes made by AJAX.  This is not a good module if you are overly concerned with SEO as there is essentially only 1 anchor page.

Now lets set up your view and blocks to make the AJAX magic happen.

5. Make a new block and put a div in it with an ID of "ajax-wrapper".
	<div id="ajax-wrapper">YOUR BLOCK'S HTML HERE</div>

	-make sure your text format is set to "Full HTML"
	-set your block region settings to "Content" and restrict the blocks content to "Only the listed pages".  Place <front> in the given space.

