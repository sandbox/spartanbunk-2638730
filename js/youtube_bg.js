(function ($) {


$(document).ready(function() {


 Drupal.behaviors.youtube_bg = {
    attach: function (context, settings) {
      console.log(Drupal.settings.youtube_bg.video_id);
      console.log(Drupal.settings.youtube_bg.start_video);
  	  console.log(Drupal.settings.youtube_bg.repeat_video);
  	  console.log(Drupal.settings.youtube_bg.mute_video);
    }
  };

// Below assigns youtube video id so that jquery tubular can make movie bg

 
 var options = {  videoId: Drupal.settings.youtube_bg.video_id, 
                   start: Drupal.settings.youtube_bg.start_video, 
                   repeat: Drupal.settings.youtube_bg.repeat_video,
				   mute:Drupal.settings.youtube_bg.mute_video,
                };  
$('#youtube_wrapper').tubular(options);

// $( ".draggable" ).draggable();
$( ".tubular-no-content" ).click(function(){
	$(".l-content").toggle(2000);
	$(".l-region--navigation").toggle(2000);
});

$( ".tubular-close" ).click(function(){
	$("#video_controls").fadeOut(1000);
	$("#maximize_tools").show(1000);
});
$( "#maximize_tools" ).click(function(){
	$("#video_controls").fadeIn(1000);
	$("#maximize_tools").hide(1000);
});

});

 })(jQuery);

